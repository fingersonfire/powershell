﻿#requires -version 4
<#
.SYNOPSIS
  Retrives the last created subdisrecory of a remote folder 

.DESCRIPTION
  Connect to a remote computer using WinSCP libary using a JSON config file and retrive the latest subfolder from a folder.
  The script creates a temporary folder localy in case the transfers fails. 
  Once the transfer is completed, the temprary folder is renamed.

.PARAMETER ConfigFile
  A JSON configuration file with all the required entries.
  You will need to double on the backslashes to avoid reading issues.
  
  Required formating : 
		{
			"LocalPath": "-",
			"RemotePath": "-",
			"HostName": "-",
			"UserName": "-",
			"Password": "-",
			"SshHostKeyFingerprint": "-",
      "FileMask" :  " | "
		}

.INPUTS
  None

.OUTPUTS
  Shows transfer errors 
  Shows successfully transfered files

.NOTES
  Version:        1.1
  Author:         KBaseio
  Creation Date:  2022.02.07
  Purpose/Change: Add FileMask option to include or exclude files from the transfer

.EXAMPLE
  Basic Example
  
  Get-SCPLastCreatedFolder.ps1 C:\Temp\ConnectionConfig.txt
  
.SOURCES 
  https://winscp.net/eng/docs/script_download_most_recent_file
  https://winscp.net/eng/docs/library_session_getfiles#powershell
  https://winscp.net/eng/docs/file_mask
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

param (
    [Parameter(Mandatory=$true,
               HelpMessage="Path to the config file that contains all the required informartion to connect")]
    [string]$ConfigFile
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'
$DebugPreference = "Continue"

#Import Modules & Snap-ins

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$WinSCPLib = "lib\WinSCP\WinSCPnet.dll"

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------




# Retrive Config file content
try{
    $ConfigFileContent = Get-Content -Path $ConfigFile
}
catch{
    Write-Error "Error while opening the file"
    exit
}

# Convert Config File content from JSON to OBJECT
try{
    $Settings = $ConfigFileContent | ConvertFrom-Json
}
catch{
    Write-Error "Error when converting JSON settings. Check your file for formating issue (Missing dougle backslashes?!?)"
    exit
}

# Main Process
try
{

    # Load WinSCP .NET assembly
    Write-Debug  "Loading WinSCP Library"
    Add-Type -Path $WinSCPLib
 
    # Setup session options
    Write-Debug "Prepare Options"
    $sessionOptions = New-Object WinSCP.SessionOptions -Property @{
        Protocol = [WinSCP.Protocol]::Sftp
        HostName = $Settings.Hostname
        UserName = $Settings.UserName
        Password = $Settings.Password
        SshHostKeyFingerprint = $Settings.SshHostKeyFingerprint
        FileMask = $Settings.FileMask
    }
 
    # Disable Buffers Optimization for this session
    # Added to prevent transfer speed to drop after a few seconds the script started
    $sessionOptions.AddRawSettings("SendBuf", "0")
 
    Write-Debug "Create Session"
    $session = New-Object WinSCP.Session
 
    try
    {
        # Connect
        Write-Debug "Opening connection"
        $session.Open($sessionOptions)
 
        # Get list of files in the directory
        Write-Debug "Listing remote path content"
        $directoryInfo = $session.ListDirectory($Settings.RemotePath)
 
        # Select the most recent file
        $latest =
            $directoryInfo.Files |
            Where-Object { $_.IsDirectory -and ($_.Name -ne ".") -and ($_.Name -ne "..") } |
            Sort-Object LastWriteTime -Descending |
            Select-Object -First 1
 
        # Any file at all?
        if ($latest -eq $Null)
        {
            Write-Host "No folder found"
            exit 1
        }

        $LocalPathFinal = Join-Path -Path $Settings.LocalPath -ChildPath $latest.Name
        $LocalPathInProgress = $LocalPathFinal + "-InProgress"
        $RemotePathSelectAll = Join-Path -Path $Settings.RemotePath -ChildPath "*"

        # Create Temp Directory
        New-Item -Path $LocalPathInProgress -ItemType Directory
        
        # Download files
        Write-Debug "Preparing Transfer Options"
        $transferOptions = New-Object WinSCP.TransferOptions
        $transferOptions.TransferMode = [WinSCP.TransferMode]::Binary
 
        # Copying Files
        Write-Debug "Copying Files"
        $transferResult = $session.GetFiles($RemotePathSelectAll, $LocalPathInProgress, $False, $transferOptions)
 
        # Throw on any error
        Write-Debug "Show Transfer Errors"
        $transferResult.Check()

        # Print transfer results
        foreach ($transfer in $transferResult.Transfers)
        {
            Write-Debug "Download of $($transfer.FileName) succeeded"
        }

        Rename-Item -Path $LocalPathFinalInProgress -NewName $LocalPathFinal

    }
    finally
    {
        # Disconnect, clean up
        $session.Dispose()
    }
 
    exit 0
}
catch
{
    Write-Host "Error: $($_.Exception.Message)"
    exit 1
}