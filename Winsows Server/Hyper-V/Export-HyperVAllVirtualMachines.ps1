﻿#requires -version 4
<#
.SYNOPSIS
  Export Virtual Machines from Hyper-V Servers

.DESCRIPTION
  Retreive a list of all virtual machines from Hyper-V server and then run the export process to a specified directory.

.INPUTS
  None

.OUTPUTS
  None

.NOTES
  Version:        1.0
  Author:         FingersOnFire
  Creation Date:  2019-05-03
  Purpose/Change: Initial script development

.EXAMPLE
  Simple Example
  
  Export-HyperVAllVirtualMachines.ps1
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins

#----------------------------------------------------------[Declarations]----------------------------------------------------------

#Any Global Declarations go here

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

# SET VARIABLES
$ArrayAllVm = @()

$arrayServer = "srv1 srv2"
$arrayServer = $arrayServer.split(" ")

# Gather all vms from Hyper-V Servers
foreach ($oCore in $arrayServer){
	$oListVmPeerHost = get-vm -ComputerName $oCore | %{ $_.Name }
	foreach ($oVm1 in $oListVmPeerHost){
		$ArrayAllVm = $ArrayAllVm += ,@($oVm1,$oCore)
	}
}

# DATE
# Get current date
$Date = Get-Date -Format dd-MMMM-yyyy_HH-mm
# Set maximum age of backups
$DelDate = (Get-Date).AddDays(-1)

#PATH
# Set backup path
$BackupPath = New-Item "C:\Export\HyperV_Export_$Date" -ItemType directory -Force
$BackupFolder = "C:\Export"



# Delete old backup files
Get-ChildItem $BackupFolder -Recurse | Where-Object {$_.LastWriteTime -lt $DelDate} | Remove-Item -Recurse
			
##
#LOOP ON EACH SERVER
for($i = 0; $i -lt $ArrayAllVm.length; $i += 1) {
#==================================================================================
	#Take info from array
	$oVM = $ArrayAllVm[$i][0]
	$oCoreSrv = $ArrayAllVm[$i][1]
	
	#export the VM
	Export-VM -computername $oCoreSrv -name $oVM -Path $BackupPath

			# Get result
			if(-not $?) {
				$Result = "Failure"
				Write-EventLog -LogName Application -Source "Backup HyperV" -EntryType Warning -EventId 2 -Message "Hyper-V VMs Export $Result. Exported VMs: $oVM"
			} else {
				$Result = "Success"
				Write-EventLog -LogName Application -Source "Backup HyperV" -EntryType Information -EventId 1 -Message "Hyper-V VMs Export $Result. Exported VMs: $oVM "
			}
			

			# Send logs via email
			$From = ""
			$To = ""
			$Subject = "Hyper-V Export Report $Date $Result"
			
			$Body = "Hyper-V Backup Report $Date <br>"
			$Body += "Result: <b>$Result</b> <br>"
			$Body += "<br>"
			$Body += "Backuped VM: $oVM <br>"
			
			$SMTPServer = ""
			$SMTPPort = "25"
			
			Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -BodyAsHtml -SmtpServer $SMTPServer -port $SMTPPort 

}