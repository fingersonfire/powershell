#requires -version 4
<#
.SYNOPSIS
  Trys to reset WSUS Authorization on a remote computer

.DESCRIPTION
  Attempts to correct WSUS computer registration by eliminating reg keys from cloning computers and prompts host to check back in to WSUS.
  
.PARAMETER ComupterName
  Specify the hostname of the computer on which the fix attempt should be made. Otherwise the script use a default value. 

.INPUTS
  None

.OUTPUTS
  LOG file if switch parameter is mentioned as parameter

.NOTES
  Version:        1.0
  Author:         kbaseio
  Creation Date:  20220325
  Purpose/Change: Initial script development
  Source : https://gist.github.com/thegooddoctorgonzo/14446d428612eaa38b460bd5b3ff2e0e

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param(

    [Parameter(Mandatory=$false)]
    [Switch]$Log

    [Parameter(Mandatory=$false)]
    [String]$ComupterName = "localhost"

    )
#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins


#----------------------------------------------------------[Declarations]----------------------------------------------------------

$LogFileExecutionPath = "C:\Temp\WSUSRepair.log"

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

if($Log) {
    Start-Transcript -Path $LogFileExecutionPath -Append -Force
}

if(Test-Connection -ComputerName $ComupterName -Verbose -Count 1)
{
    Invoke-Command -ComputerName $ComupterName -ScriptBlock {Stop-Service -Name wuauserv -Verbose } -Verbose

    #Delete reg keys
    Invoke-Command -ComputerName $ComupterName -ScriptBlock {Remove-ItemProperty -Name AccountDomainSid -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate  -Verbose } -Verbose
    Invoke-Command -ComputerName $ComupterName -ScriptBlock {Remove-ItemProperty -Name PingID -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate  -Verbose } -Verbose
    Invoke-Command -ComputerName $ComupterName -ScriptBlock {Remove-ItemProperty -Name SusClientId -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate  -Verbose } -Verbose
    Invoke-Command -ComputerName $ComupterName -ScriptBlock {Remove-ItemProperty -Name SusClientIDValidation -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\WindowsUpdate\ -Verbose } -Verbose

    #Delete files
    $path = "\\" + $ComupterName + "\c$\Windows\SoftwareDistribution"
    Remove-Item -Path $path -Recurse -Verbose 

    $path = "\\" + $ComupterName + "\c$\Windows\WindowsUpdate.log"
    Remove-Item -Path $path -Verbose 

    #Restart update service and force check in to WSUS
    Invoke-Command -ComputerName $ComupterName -ScriptBlock {Start-Service -Name wuauserv -Verbose } -Verbose
    Invoke-Command -ComputerName $ComupterName -ScriptBlock { & cmd.exe /c wuauclt /resetauthorization /detectnow} -Verbose
}

if($Log) {
    Stop-Transcript
}