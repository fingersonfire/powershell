﻿#requires -version 4

<#
.SYNOPSIS
  Install Printer using powershell

.DESCRIPTION
  This Powershell InTune script allows you to download the driver and configure a printer
  Initial Source : https://social.technet.microsoft.com/Forums/en-US/fdfa7560-124f-4967-a51e-2a8f5621e5c2/deploy-a-printer-through-intune-an-easy-way?forum=microsoftintuneprod

.PARAMETER Install-Printer.ps1
  

.INPUTS
  

.OUTPUTS
  

.NOTES
  Version:        1.0
  Author:         fingersonfire
  Creation Date:  2020/08/12
  Purpose/Change: Initial script development

.EXAMPLE
  Install-Printer.ps1
  
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set-ExecutionPolicy remotesigned

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$url = "https://www.support.xerox.com/support/versalink-c405/file-redirect/enus.html?operatingSystem=win10x64&fileLanguage=en&contentId=144292"
$file = "c:\temp\VersaLink_C400_C405_7.95.0.0_PCL6_x64.zip"
$filename = "VersaLink_C400_C405_7.95.0.0_PCL6_x64.zip"

$printerDriverName = "Xerox VersaLink C405 V4 PCL6"
$printerName = "Xerox Versa C405"
$printerIP = "IP_192.168.61.99"
$printerPort = "IP_192.168.61.99_XV"

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------



#Download file
$clnt = new-object System.Net.WebClient
$clnt.DownloadFile($url, $file)


#Unzip file
$shell_app=new-object -com shell.application 
$zip_file = $shell_app.namespace("C:\temp" + "\$filename") 
$destination = $shell_app.namespace("C:\temp") 
$destination.Copyhere($zip_file.items())

#Install Printer
Invoke-Command {pnputil.exe -a "C:\Temp\XeroxVersaC405\Xerox_VersaLink_C400_C405_PCL6.inf" }
Add-PrinterDriver -Name $printerDriverName
Get-PrinterDriver

Add-PrinterPort -Name $printerPort -PrinterHostAddress $printerIP
Start-Sleep 10
Add-Printer -Name $printerName -ShareName $printerName  -PortName $printerPort -DriverName $printerDriverName