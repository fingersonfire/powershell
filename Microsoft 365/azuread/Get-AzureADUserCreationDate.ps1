#requires -version 4
<#
.SYNOPSIS
  Retreive creation date for each users from Azure AD

.DESCRIPTION
  

.PARAMETER <Parameter_Name>
  <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  <Inputs if any, otherwise state None>

.OUTPUTS
  <Outputs if any, otherwise state None>

.NOTES
  Version:        1.0
  Author:         KBaseio
  Creation Date:  2022.04.27
  Purpose/Change: Initial script development

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$Report = @()

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------



$AAD_users = Get-AzureADUser -All:$true

foreach ($AAD_User in $AAD_users) {
  $objReport = [PSCustomObject]@{
      User = $AAD_User.UserPrincipalName
      CreationDate = (Get-AzureADUserExtension -ObjectId $AAD_User.ObjectId).Get_Item("createdDateTime")
  }
  
  $Report += $objReport
}

$Report