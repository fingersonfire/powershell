#requires -version 4
<#
.SYNOPSIS
  Forces all user to sign is again

.DESCRIPTION
  Clears all currently open user session to force users to sign it again

.PARAMETER <Parameter_Name>
  <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  <Inputs if any, otherwise state None>

.OUTPUTS
  <Outputs if any, otherwise state None>

.NOTES
  Version:        1.0
  Author:         kbaseio
  Creation Date:  20220627
  Purpose/Change: Initial script development

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins

#Check for AzureAD module
$Modules=Get-Module -Name AzureAD -ListAvailable
if($Modules.count -eq 0)
{
  Write-Host  Please install AzureAD module using below command: `nInstall-Module AzureAD
  Exit
}

Import-Module AzureAD

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$UserSkipped = @(
    ""
)

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

Connect-AzureAD

# To get all the users
$users = Get-AzureADUser -All $true | Where-Object {$_.UserType -eq 'Member'};

foreach ( $user in $users){
    if(-Not $UserSkipped.Contains($user.UserPrincipalName)){
        Write-Host $user.UserPrincipalName
        Revoke-AzureADUserAllRefreshToken -ObjectId $user.UserPrincipalName
    }
}