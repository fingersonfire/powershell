try { 
	$var = Get-AzureADTenantDetail 
} 
catch [Microsoft.Open.Azure.AD.CommonLibrary.AadNeedAuthenticationException] { 
	Write-Host "You're not connected to AzureAD";  
	Write-Host "Make sure you have AzureAD mudule available on this system then use Connect-AzureAD to establish connection";  
	exit;
}

$users = Get-AzureADUser -Filter "UserType eq 'Guest'" -All $true

$usersInfo = @()

foreach($user in $users){

	$usersInfo += [PSCustomObject]@{
	DisplayName = $user.DisplayName
	Mail = $user.Mail
	CreationType = $user.CreationType
	createdDateTime = $user.ExtensionProperty.createdDateTime
	userState = $user.ExtensionProperty.userState
	userStateChangedOn = $user.ExtensionProperty.userStateChangedOn
	}

}

$usersInfo