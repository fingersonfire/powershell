#requires -version 4
<#
.SYNOPSIS
  Force Password change for all user upon next login

.DESCRIPTION
  Set the passwoird should be changed upon next login without changing the current password. 

.PARAMETER <Parameter_Name>
  <Brief description of parameter input required. Repeat this attribute if required>

.INPUTS
  <Inputs if any, otherwise state None>

.OUTPUTS
  <Outputs if any, otherwise state None>

.NOTES
  Version:        1.0
  Author:         kbaseio
  Creation Date:  20220627
  Purpose/Change: Initial script development

.EXAMPLE
  <Example explanation goes here>
  
  <Example goes here. Repeat this attribute for more than one example>
#>

#---------------------------------------------------------[Script Parameters]------------------------------------------------------

Param (
  #Script parameters go here
)

#---------------------------------------------------------[Initialisations]--------------------------------------------------------

#Set Error Action to Silently Continue
$ErrorActionPreference = 'SilentlyContinue'

#Import Modules & Snap-ins

#Check for MSOnline module
$Modules=Get-Module -Name MSOnline -ListAvailable
if($Modules.count -eq 0)
{
  Write-Host  Please install MSOnline module using below command: `nInstall-Module MSOnline  -ForegroundColor yellow
  Exit
}

Import-Module MSOnline

#----------------------------------------------------------[Declarations]----------------------------------------------------------

$UserSkipped = @(
    ""
)

#-----------------------------------------------------------[Functions]------------------------------------------------------------



#-----------------------------------------------------------[Execution]------------------------------------------------------------

Connect-MSOLService

# To get all the users
$users = Get-MsolUser

foreach ($user in $users){
    if(-Not $UserSkipped.Contains($user.UserPrincipalName)){
        Write-Host $user.UserPrincipalName
        Set-MsolUserPassword -UserPrincipalName $user.UserPrincipalName -ForceChangePassword:$true -ForceChangePasswordOnly:$true
    }
}